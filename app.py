from flask import Flask, jsonify, request
from sqlalchemy import create_engine
from sqlalchemy.sql import text
import json

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

conn_str = 'postgresql://postgres:Omron_306043@localhost:5432/coffee3'
engine = create_engine(conn_str, echo=False)

@app.route('/index', methods=['GET'])
def greetings():
    return "Hello"

@app.route('/user', methods=['GET'])
def get_users():
    all = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM public.user ORDER BY user_id")
            result = connection.execute(qry)
            for item in result:
                all.append({
                    'user_id':item[0], 'name':item[1], 'email':item[2], 'phone':item[3], 'address':item[4]
                })
            return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/user/add', methods=['POST'])
def add_user():
    body_req = request.json
    name = body_req.get('name')
    email = body_req.get('email')
    phone = body_req.get('phone')
    address = body_req.get('address')
    try:
        with engine.connect() as connection:
            qry = text("INSERT INTO public.user (name, email, phone, address)\
                        VALUES (:name, :email, :phone, :address)")
            connection.execute(qry, name=name, email=email, phone=phone, address=address)
            new_qry = text("SELECT * FROM public.user WHERE name=:name")
            new_result = connection.execute(new_qry, name=name)
            all = []
            for item in new_result:
                all.append({
                    'user_id':item[0], 'name':item[1], 'email':item[2], 'phone':item[3], 'address':item[4]
                    })
            return jsonify(status='succecc create account', data=all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/user/update/<_id>', methods=['PUT'])
def update_user(_id):
    body_req = request.json
    user_id = _id
    name = body_req.get('name')
    email = body_req.get('email')
    phone = body_req.get('phone')
    address = body_req.get('address')
    try:
        with engine.connect() as connection:
            qry = text("UPDATE public.user SET name=:name, email=:email, phone=:phone, address=:address \
                        WHERE user_id=:user_id")
            connection.execute(qry, name=name, email=email, phone=phone, address=address, user_id=user_id)
            new_qry = text(" SELECT * FROM public.user WHERE name=:name ")
            new_result = connection.execute(new_qry, name=name)
            all = []
            for item in new_result:
                all.append({
                'user_id':item[0], 'name':item[1], 'email':item[2], 'phone':item[3], 'address':item[4]        
                })
            return jsonify(status='success update',data=all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/menu', methods=['GET'])
def get_menus():
    all = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM public.menu")
            result = connection.execute(qry)
            for item in result:
                all.append({
                    'menu_id':item[0], 'menu':item[1], 'description':item[2], 'price':item[3], 'stock':item[4]
                })
            return(jsonify(all))
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/menu-stock', methods=['GET'])
def get_menu_in_stock():
    all = []
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM public.menu WHERE stock > 0")
            result = connection.execute(qry)
            for item in result:
                all.append({
                    'menu_id':item[0], 'menu':item[1], 'description':item[2], 'price':item[3], 'stock':item[4]
                })
            return(jsonify(all))
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/menu/add', methods=['POST'])
def add_menu():
    body_req = request.json
    menu_id = body_req.get('menu_id')
    menu = body_req.get('menu')
    description = body_req.get('description')
    price = body_req.get('price')
    stock = body_req.get('stock')
    try:
        with engine.connect() as connection:
            qry = text("INSERT INTO public.menu (menu_id, menu, description, price, stock) \
                        VALUES (:menu_id, :menu, :description, :price, :stock)")
            connection.execute(qry, menu_id=menu_id, menu=menu, description=description, price=price, stock=stock)
            new_qry = text("SELECT * FROM public.menu WHERE menu_id=:menu_id")
            result = connection.execute(new_qry, menu_id=menu_id)
            new_menu = []
            for item in result:
                new_menu.append({
                'menu_id':item[0], 'menu':item[1], 'description':item[2], 'price':item[3], 'stock':item[4]        
                })
            return jsonify(status="success add new menu", new_menu=new_menu)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/menu/update/<menu_id>', methods=['PUT'])
def update_menu(menu_id):
    body_req = request.json
    menu_id = menu_id
    menu = body_req.get('menu')
    description = body_req.get('description')
    price = body_req.get('price')
    stock = body_req.get('stock')
    try:
        with engine.connect() as connection:
            qry = text("UPDATE public.menu SET menu=:menu, description=:description, price=:price, stock=:stock \
                        WHERE menu_id=:menu_id ")
            connection.execute(qry, menu=menu, description=description, price=price, stock=stock, menu_id=menu_id)
            new_qry = text("SELECT * FROM public.menu WHERE menu_id=:menu_id")
            new_result = connection.execute(new_qry, menu_id=menu_id)
            all = []
            for item in new_result:
                all.append({
                'menu_id':item[0], 'menu':item[1], 'description':item[2], 'price':item[3], 'stock':item[4]        
                })
            return jsonify(status='success update',data=all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/menu/update-stock', methods=['PUT'])
def update_stock():
    body_req = request.json
    menu_id = body_req.get('menu_id')
    stock = body_req.get('stock')
    try:
        with engine.connect() as connection:
            qry = text("UPDATE public.menu SET stock=(SELECT stock FROM menu \
                        WHERE menu_id=:menu_id)+:stock WHERE menu_id=:menu_id")
            connection.execute(qry, stock=stock, menu_id=menu_id)
            new_qry = text("SELECT * FROM public.menu WHERE menu_id=:menu_id")
            result = connection.execute(new_qry, menu_id=menu_id)
            new_stock = []
            for item in result:
                new_stock.append({
                    'menu_id':item[0], 'menu':item[1], 'description':item[2], 'price':item[3], 'stock':item[4]
                })
            return jsonify(status='success update stock', data=new_stock)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/order', methods=['GET'])
def get_orders():
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM public.order ORDER BY order_id")
            result = connection.execute(qry)
            all = []
            for item in result:
                all.append({
                    'order_id':item[0], 'user_id':item[1], 'status':item[2], 'order_date':item[3]
                })
            return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/add-order', methods=['POST'])
def add_order():
    body_req = request.json
    user_id = body_req.get('user_id')
    try:
        with engine.connect() as connection:
            qry1 = text("SELECT count(status) as status FROM public.order \
                        WHERE status ='process' AND user_id=:user_id GROUP BY status")
            result = connection.execute(qry1, user_id=user_id)
            active = [item for item in result]

            if active == [] or active[0][0] < 10:
                qry2 = text("INSERT INTO public.order (user_id, status) VALUES (:user_id, :status)")
                connection.execute(qry2, user_id=user_id, status='process')
                qry3 = text("SELECT * FROM public.order ORDER BY order_id DESC LIMIT 1")
                result3 = connection.execute(qry3)
                new_order = []
                for item in result3:
                    new_order.append({
                        'order_id':item[0], 'user_id':item[1], 'status':item[2], 'order_date':item[3]
                    })
                return jsonify(data=new_order)
            else:
                return jsonify(status='Maximum Order')
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/order/complete/<order_id>', methods=['PUT'])
def update_to_complete(order_id):
    order_id = order_id
    try:
        with engine.connect() as connection:
            qry1 = text("UPDATE public.order SET status='complete' WHERE order_id=:order_id")
            connection.execute(qry1, order_id=order_id)
            qry2 = text("SELECT * FROM public.order WHERE order_id=:order_id")
            result = connection.execute(qry2, order_id=order_id)
            status = []
            for item in result:
                status.append({
                    'order_id':item[0], 'user_id':item[1], 'status':item[2]
                })
            return jsonify(status)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/order/cancel/<order_id>', methods=['PUT'])
def update_to_cancel(order_id):
    order_id = order_id
    try:
        with engine.connect() as connection:
            qry1 = text("UPDATE public.order SET status='cancel' WHERE order_id=:order_id")
            connection.execute(qry1, order_id=order_id)
            qry2 = text("SELECT * FROM public.order WHERE order_id=:order_id")
            result = connection.execute(qry2, order_id=order_id)
            status = []
            for item in result:
                status.append({
                    'order_id':item[0], 'user_id':item[1], 'status':item[2]
                })
            return jsonify(status)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/order-detail', methods=['POST'])
def order_detail():
    body_req = request.json
    order_id = body_req.get('order_id')
    menu_id = body_req.get('menu_id')
    quantity = body_req.get('quantity')
    try:
        with engine.connect() as connection:
            # Enter the detail with price 0
            for index, item in enumerate(menu_id):
                qry = text("INSERT INTO public.bucket (order_id, menu_id, quantity, price) \
                            VALUES (:order_id, :menu_id, :quantity, :price)")
                connection.execute(qry, order_id=order_id, menu_id=item, quantity=quantity[index], price=0)
            # Calculate the price depend on quantity
            for item in range(len(menu_id)):
                qry = text("UPDATE public.bucket SET price=(SELECT price FROM public.menu \
                            WHERE menu_id=:menu_id)*quantity WHERE order_id=:order_id AND menu_id=:menu_id")
                connection.execute(qry, order_id=order_id, menu_id=menu_id[item])
            # Update/substrack stock in menu table depend on quantity
            for item in range(len(menu_id)):
                qry = text("UPDATE public.menu SET stock=stock-(SELECT quantity FROM public.bucket \
                            WHERE order_id=:order_id AND menu_id=:menu_id) WHERE menu_id=:menu_id")
                connection.execute(qry, order_id=order_id, menu_id=menu_id[item])
            qry = text("SELECT public.order.order_id, public.order.user_id, public.user.name, SUM(public.bucket.price) FROM public.bucket \
                        JOIN public.order USING(order_id) JOIN public.user USING(user_id) \
                        WHERE public.bucket.order_id=:order_id GROUP BY public.order.order_id, public.user.name \
                        ORDER BY order_id")
            result = connection.execute(qry, order_id=order_id)
            detail = []
            for item in result:
                detail.append({
                    'order_id':item[0], 'user_id':item[1], 'name':item[2], 'total':item[3]
                })
            return jsonify(detail)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/bucket', methods=['GET'])
def get_bucket():
    try:
        with engine.connect() as connection:
            qry = text("SELECT * FROM public.bucket \
                        GROUP BY order_id, menu_id \
                        ORDER BY order_id")
            result = connection.execute(qry)
            all = []
            for item in result:
                all.append({
                    'order_id':item[0], 'menu_id':item[1], 'quantity':item[2], 'price':item[3]
                })
            return jsonify(all)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/bucket/<id>', methods=['GET'])
def get_bucket_using_id(id):
    id = id
    try:
        with engine.connect() as connection:
            qry = text("SELECT public.bucket.order_id, public.bucket.menu_id, public.bucket.quantity, \
                        public.menu.price as price_per_item, public.bucket.price as total FROM public.bucket \
                        JOIN public.menu USING(menu_id) WHERE order_id=:order_id")
            result = connection.execute(qry, order_id=id) 
            # all = []
            # for item in result:
            #     all.append({
            #         'menu_id':item[1], 'quantity':item[2], 'price':item[3]
            #     })
            # order_id = item[0]
            # return jsonify(order_id=order_id, data=all)
            menu, qty, price, total = [], [], [], []
            for item in result:
                menu.append(item[1])
                qty.append(item[2])
                price.append(item[3])
                total.append(item[4])
            return jsonify({'order_id':item[0], 'menu_id':menu, 'quantity':qty, 'price':price, 'total':total})
    except Exception as e:
        return jsonify(error=str(e))

# Top 5 menus who is ordered frequently and complete
@app.route('/top-5/menu', methods=['GET'])
def top_5_menu_ordered():
    try:
        with engine.connect() as connection:
            qry = text("SELECT public.menu.menu_id, public.menu.menu, count(public.bucket.menu_id) as sum \
                        FROM public.order JOIN public.bucket USING(order_id) \
                        JOIN public.menu USING(menu_id) WHERE public.order.status='complete' \
                        GROUP BY public.menu.menu_id, public.menu.menu \
                        ORDER BY sum DESC LIMIT 5") 
            result = connection.execute(qry)
            top_5 = []
            for item in result:
                top_5.append({
                    'menu_id':item['menu_id'], 'menu':item['menu'], 'sum':item['sum']
                })
            return jsonify(top_5)
    except Exception as e:
        return jsonify(error=str(e))

# Top 5 users who order frequently and complete
@app.route('/top-5/user', methods=['GET'])
def top_5_user_ordered():
    try:
        with engine.connect() as connection:
            qry = text("SELECT public.user.user_id, public.user.name, count(public.order.user_id) as total_transaction \
                        FROM public.order JOIN public.user USING (user_id) \
                        WHERE status='complete' GROUP BY public.user.name,  public.user.user_id \
                        ORDER BY total_transaction DESC LIMIT 5")
            result = connection.execute(qry)
            top_5 = []
            for item in result:
                top_5.append({
                    'user_id':item['user_id'], 'name':item['name'], 'total_transaction':item['total_transaction']
                })
            return jsonify(top_5)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/check-active', methods=['GET'])
def check_active():
    try:
        with engine.connect() as connection:
            qry = text("SELECT user_id, name, count(status) as service FROM public.order \
                        JOIN public.user USING(user_id) WHERE status='process' GROUP BY user_id, name ORDER BY user_id")
            result = connection.execute(qry)
            process = []
            for item in result:
                process.append({
                    'user_id':item[0], 'name':item[1], 'service':item[2]
                })
            if process == []:
                return jsonify(process=0)
            else:
                return jsonify(process)
    except Exception as e:
        return jsonify(error=str(e))